package EsercizioManuale.esercizioQuattro;

public class CambiaRisultato {


    public void cambiaRisultato(Risultato numero) //input che ha in sè 2.0F(risultato)
    {
        numero.risultato= numero.risultato +2.5F;  // accedo al suo attributo e gli aggiungo 2.5F;
    }

    public float cambiaRisultato(float risultato)
    {
        risultato= risultato+3.0F;
        return risultato;
    }
}