package EsercizioManuale.esercizioQuattro;

public class TestRisultato
{
    public static void main(String[] args)
    {
        Risultato numero = new Risultato(2.0F);
        numero.stampaRisultato(); //= 2.0F
        CambiaRisultato numero2 = new CambiaRisultato();
        numero2.cambiaRisultato(numero); // numero=2.0F (+2.5F)
        numero.stampaRisultato();
    }
}
