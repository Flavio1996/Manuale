package EsercizioManuale.EsercizioDieci;

import java.util.Scanner;

public class ProgrammaInterattivo
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);// oggetto che rappresenta la tastiera del nostro calcolatore
        String stringa = "";
        System.out.println("digita qualcosa e batti enter");
        while (!(stringa=scanner.next()).equals("fine")) // cio' che vado a scrivere da tastiera deve essere diverso da fine per stamparmi la stringa che ho scritto da console
        {
            System.out.println("Hai digitato  "+stringa.toUpperCase()+"!");
        }
        System.out.println("Fine programma!");//se invece la stringa è uguale a "fine" , allora uscirà dal ciclo e mi stamperà "fine programma"

        // next() blocca l'esecuzione del codice finchè non gli passo una stringa da tastiera.
    }
}
