package EsercizioManuale.esercizioQuindici;


public class Gara
{
    private String nome; //nome gara
    private String risultato; // chi ha vinto
    private Auto[] griglia;

    public Gara(String nome)
    {
        setNome(nome);
        setRisultato("corsa non terminata");
        creaGrigliaDiPartenza();
    }

    public void creaGrigliaDiPartenza()
    {
        Pilota pilota1= new Pilota("marquez"); // creo i piloti
        Pilota pilota2= new Pilota("flavio");
        Pilota pilota3= new Pilota("Ramon");
        Pilota pilota4= new Pilota("gonzales");
        Auto auto1 = new Auto("mercedes",pilota1); // creo le auto per i piloti
        Auto auto2 = new Auto("renault",pilota2);
        Auto auto3 = new Auto("ford",pilota3);
        Auto auto4 = new Auto("ferrari",pilota3);
        griglia= new Auto[4];
        griglia[0]=auto1;  // vengono messe le macchine (con i piloti dentro) nelle rispettive posizioni della griglia
        griglia[1]=auto2;
        griglia[2]=auto3;
        griglia[3]=auto4;
    }

    public void corriGara()
    {
        int numeroVincente= (int)(Math.random()*4);
        Auto vincitore= griglia[numeroVincente];
        String risultato = vincitore.dammiDettagli();
        setRisultato(risultato);
    }


    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }



    public void setRisultato(String vincitore)
    {
        this.risultato = "il vincitore di "+getNome()+  "è :"+ vincitore;
    }

    public String getRisultato()
    {
        return risultato;
    }
}
