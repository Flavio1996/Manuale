package EsercizioManuale.esercizioQuindici;

public class Auto {
    public String scuderia;
    public Pilota pilota;

    public Auto(String scuderia, Pilota pilota)
    {
       SetScuderia(scuderia);
       SetPilota(pilota);
    }

    private void SetPilota(Pilota pilota)
    {
        this.pilota=pilota;
    }

    public Pilota getPilota()
    {
        return pilota;
    }

    private void SetScuderia(String scuderia)
    {

        this.scuderia=scuderia;
    }

    public String getScuderia()
    {
        return scuderia;
    }




    public String dammiDettagli()
    {
        return getPilota().getNome()+ "su"+ getScuderia();

    }
}
