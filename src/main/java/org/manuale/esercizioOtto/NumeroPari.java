package EsercizioManuale.esercizioOtto;

public class NumeroPari {
    public static void main(String[] args) {
        int indice= 0;

        while(true)
        {
            indice ++; // aggiungerebbe 1 al'infinito perchè la condizione è impostata sul true
            if(indice>10)
                break;
            if((indice%2)!= 0) // perchè se il resto della divisione è diverso da 0 allora vuol dire che è un num disp
                continue;
            System.out.println(indice);
        }

    }
}
