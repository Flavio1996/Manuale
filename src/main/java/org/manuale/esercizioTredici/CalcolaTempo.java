package EsercizioManuale.esercizioTredici;

import java.util.Scanner;

public class CalcolaTempo
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        final long MINUTI_GIORNO=1440L; //i minuti che sono presenti in 24h
        System.out.println("Quanti giorni sono passati dalla tua ultima vacanza?");
        int numerogiorni=scanner.nextInt();
        System.out.println("vuoi sapere a quanti minuti corrispondono?");
        if(scanner.next().equals("ok"))
        {
            System.out.println("sono passati esattamente "+numerogiorni*MINUTI_GIORNO+" minuti!");
        }
    }
}
