package org.manuale.esercizioSedici;


public class PortaMonete {
    /**
     * ho creato un array che può contenere un numero limitato di monete (10 monete)!
     *
     * farà da contenitore alle monete..
     */
    private final Moneta monete[] = new Moneta[10];


    /**
     * FIXME
     *
     * @param valori
     *
     * se le monete aggiunte sono più di 10 dice che il portamonete è pieno (non stampa le monete aggiunte!)
     */

    public PortaMonete(int... valori) {
        int numeromonete = valori.length;
        for (int i = 0; i < numeromonete; i++) {
            if (numeromonete >= 10) {
                System.out.println("Sono state inserite solo le prime 10 monete");
                break;
            }
            monete[i] = new Moneta(valori[i]); //ho inserito n monete nel portamonete
        }

    }

    public void stato() {
        System.out.println("il portamonete contiene: ");
        for (Moneta moneta : monete) {
            if (moneta == null) {
                break;
            }
            System.out.println("una " + moneta.getDescrizione());
        }
    }

    public Moneta preleva(Moneta moneta) {
        System.out.println("provimo a prelevare una " + moneta.getDescrizione());
        Moneta monetaTrovata = null;  //nel caso in cui il portamonete sia vuoto
        int indiceMonetaTrovata = indiceMonetaTrovata(moneta);
        if (indiceMonetaTrovata == -1) {
            System.out.println("moneta non trovata");
        } else {
            monetaTrovata = moneta;
            monete[indiceMonetaTrovata] = null; // viene reso null perchè la moneta viene prelevata ([0]==null)
            System.out.println("una " + moneta.getDescrizione() + " prelevata");
        }
        return  monetaTrovata;
    }



    private int indiceMonetaTrovata(Moneta moneta)
    {
        int indiceMonetaTrovata = -1;
        for (int i = 0; i < 10; ++i) {
            if (monete[i] == null) {
                break;
            }
            int valoreMonetaNelPortaMoneta = monete[i].getValore();
            int valoreMonetaDaPrelevare = moneta.getValore();
            if (valoreMonetaDaPrelevare == valoreMonetaNelPortaMoneta) {
                indiceMonetaTrovata = i;
                break;
            }
        }
        return indiceMonetaTrovata;
    }


    public void aggiungi(Moneta moneta)
    {
        System.out.println("proviamo ad aggiungere una " +moneta.getDescrizione());
        int indicelibero= primoIndiceLibero();
        if(indicelibero==-1)
        {
            System.out.println("portamonete pieno! la moneta non è stata aggiunta!");
        }else{
            monete[indicelibero]=moneta; //aggiungo la moneta;
            System.out.println("La moneta è stata aggiunta con successo");
        }

    }

    /**
     *
     * @return il primo indice libero che trova per inserire la moneta..
     */
    private int primoIndiceLibero()
    {
        int indicelibero=-1;
        for(int i = 0; i<10; i++)
        {
            if(monete[i]==null)
            {
                indicelibero = i;
                break;
            }

        }
      return indicelibero;
    }
}
