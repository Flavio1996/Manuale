package org.manuale.esercizioSedici;

public class Moneta
{
    private static final String VALUTA= "€";
    private int valore;

    public Moneta(int valore)
    {
      this.valore=valore;
      System.out.println("E'stata creata una " +getDescrizione());
    }

    private String formattaStringaDescrittiva(int valore)
    {
        String formattastringa = " centesimi di ";
        if(valore==1)
        {
           formattastringa = " centesimo di ";
        }else if(valore>99)
        {
            valore= valore/100;
            formattastringa=" ";
        }
        return  valore + formattastringa;
    }

    public String getDescrizione()
    {
        String descrizione= "moneta da " + formattaStringaDescrittiva(valore)+VALUTA;
        return  descrizione;
    }

    public int getValore()
    {
        return valore;
    }
}
