package org.manuale.esercizioSei;

import java.util.Arrays;

public class Alfabeto
{
    public static void main(String[] args)
    {
        char alfabeto [] = {'a', 'b', 'c', 'd', 'e', 'f'};
        System.out.println(Arrays.toString(alfabeto));
    }
}