package EsercizioManuale.esercizioQuattordici;

public class TestCalcolo
{
    public static void main(String[] args) {
        CalcolatriceSemplificata test = new CalcolatriceSemplificata();
        System.out.println(test.somma());
        System.out.println(test.moltiplicazione());
        System.out.println(test.divisione());
        System.out.println(test.media());
        System.out.println(test.resto());
        System.out.println(test.sottrazione());
        System.out.println(test.sottrazionePlus());
        test.dammiIlMaggiore();
        test.dammiIlMinore();

    }
}
