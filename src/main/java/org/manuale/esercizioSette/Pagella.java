package EsercizioManuale.esercizioSette;

import java.util.Arrays;

public class Pagella
{
    public Studente studente;
    public String [][] tabellaVoti;

    public Pagella(Studente s,String [][] tv)
    {
        studente = s;
        tabellaVoti= tv;
    }

     public void stampaPagella() // vado a crare solamente la struttura della pagella
     {
         System.out.println(studente.toString());
         System.out.println(Arrays.toString(tabellaVoti[0]));
         System.out.println(Arrays.toString(tabellaVoti[1]));
         System.out.println(Arrays.toString(tabellaVoti[2]));
         System.out.println(Arrays.toString(tabellaVoti[3]));
         System.out.println(Arrays.toString(tabellaVoti[4]));
         System.out.println(Arrays.toString(tabellaVoti[5]));
     }
}
