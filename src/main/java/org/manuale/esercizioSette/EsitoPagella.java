package EsercizioManuale.esercizioSette;

public class EsitoPagella
{
    public static void main(String[] args)
    {
        Studente studente1 = new Studente("Flavio","Catuara","B");
        String tabellaVoti1[][]= {
                {"Italiano","7","Buono"},
                {"Storia","7","Buono"},
                {"Inglese","9","Distinto"},
                {"Matematica","5","non sufficiente"},
                {"Geografia","6","Sufficiente"},
                {"Spagnolo","10","Ottimo"}
        };
        Pagella pagella1 = new Pagella(studente1,tabellaVoti1);

        pagella1.stampaPagella();
    }
}
