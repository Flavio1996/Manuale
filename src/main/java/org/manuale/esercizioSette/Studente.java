package EsercizioManuale.esercizioSette;

public class Studente   // la clsse studente definisce nome,cognome e classe
{
    String nome;
    String cognome;
    String classe;


    public Studente(String n, String c, String cl)
    {
        nome=n;
        cognome=c;
        classe=cl;
    }
    public String toString()
    {
        return "Studente: " +nome+ " " +cognome+ "\nClasse: " +classe;

    }
}