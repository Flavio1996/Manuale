package EsercizioManuale.esercizioDue;

public class Persona
{
    public String nome;
    public String cognome;
    public int eta;

    /** Questo è un metodo //primo javadoc
     *
     * @return
     */

    public String dettagli()
    {
        return (nome +" " + cognome +" anni " +eta);
    }
}
