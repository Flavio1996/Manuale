package EsercizioManuale.esercizioUnidici;

public class PrePostIncremento
{
    public static void main(String[] args)
    {
     int i = 99;
     //durante la verifica i=99 e solo dopo int i viene incrementato a 100( op.post incremento ha minore priorità verso operatore di relazione)
     if(i++>=100)
     {
         System.out.println(i+=10);
     }else
         {
             System.out.println(--i==99?i++:++i);//operatore ternario
         }
    }
}
